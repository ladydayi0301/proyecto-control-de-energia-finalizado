const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TaskSchema = Schema({
    title: String,
    nombre_electrodomestico: String,
    voltaje: String,
    corriente: String,
    frecuencia:String,
    consumo:String,
    angulo:String,
    status: {
      type: Boolean,
      default: false
    }
  });

  module.exports = mongoose.model('tasks', TaskSchema);